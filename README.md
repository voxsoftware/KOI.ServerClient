# KOI.ServerClient

KOI.ServerClient
Aplicación "cliente" para extraer los datos. 



## Instalación en windows, mac, linux (modo git y siempre actualizado)

Para instalar en windows se recomienda usar [cmder full version](http://cmder.net/) o algún otro bash con git instalado
Ubíquese en una carpeta donde quiera que vayan los proyectos

* Instale [nodejs](https://nodejs.org) versión 8 o superior

* Instale pm2 por medio de consola (En windows use cmd o cmder en modo administrador)

```sh 
npm -g install pm2 
```

* Baje los proyectos por medio de git (cmder en windows)

```sh 



git clone https://github.com/voxsoftware/vox-core 


git clone https://gitlab.com/voxsoftware/kowix
cd kowix 
npm install 
cd ..


git clone https://gitlab.com/voxsoftware/KOI.ServerClient

cd KOI.ServerClient 
cp config.example.json config.json 
cp kowix.json ..

```

* Edite el archivo config.json de acuerdo a los parámetros que necesite


