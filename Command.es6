/*global core*/
import Path from 'path'
var Fs = core.System.IO.Fs

var Command = new core.VW.CommandLine.Parser()
import Child from 'child_process'
import Os from 'os'

Command.addParameter('aa', true, 0)
Command.addParameter('mm', true, 0)
Command.addParameter('dd', true, 0)
Command.addParameter('doc', true, 0)

Command.addParameter('tarea', true, "SRI-RECIBIDOS")
Command.addParameter('diario', true, false)

Command.addParameter('force', true, false)
Command.addParameter('forzar', true, false)


Command.addParameter('empresa', true, 0)
Command.parse(null, false)


var ejecutarProceso = async function() {

	var config = require("./config.json")
	var er, numcpus
	try {
		var req = new core.VW.Http.Request("http://127.0.0.1:" + config.port + "/a")
		await req.getResponseAsync()
	}
	catch (e) {
		er = e
	}
	finally {
		if (req && req.innerRequest)
			delete req.innerRequest.callback
	}

	if (er) {



		// start pm2 service 

		var copyEnv = {}
		for (var id in process.env) {
			copyEnv[id] = process.env[id]
		}
		copyEnv.SERVER_PORT = config.port

		numcpus = config.cpus || Os.cpus().length
		var pathKowix = Path.join(__dirname, "..", "kowix")
		if (!Fs.sync.exists(pathKowix))
			pathKowix = Path.join(__dirname, "..", "..", "kowix")

		pathKowix = Path.join(pathKowix, "manualstart.js")
		var executed = false,
			pathPM2, task
		if (Os.platform() == "win32") {
			pathPM2 = Path.join(process.env.APPDATA, "npm", "node_modules", "pm2")
			if (Fs.sync.exists(pathPM2)) {
				var pm2 = require(pathPM2)
				task= new core.VW.Task()
				
				pm2.connect(function(err) {
					if (err) {
						task.exception= err 
						return task.finish()
					}
					process.env.SERVER_PORT= config.port
					pm2.start({
						name: config.pname || "kowix",
						script: pathKowix, // Script to be run
						exec_mode: 'cluster', // Allows your app to be clustered
						instances: numcpus
					}, function(err, apps) {
						pm2.disconnect() 
						if (err)
							task.exception= err 
						
						task.finish()
					})
					
				})
				await task 
				executed= true
				
			}
		}

		if(!executed){
			var sp = Child.spawn("pm2", ["start", pathKowix, "--name", config.pname || "kowix"], {
				detached: true,
				env: copyEnv
			})
			sp.unref()
		}
		
		// wait a time 
		vw.log("> Iniciando servicio KOI.ES para esta y posteriores ejecuciones (debe tener instalado pm2 > npm install -g pm2)")
		await core.VW.Task.sleep(5000)
	}




	// execute task ...
	try {
		var pars = Command.getAllParameters()
		
		var tarea ,d
		if(pars.diario!==false){
			
			vw.info("> Ejecutando como tarea")
			d= core.VW.Moment().startOf('day').add(-1,'day').toDate()
			tarea= true
			if(!pars.aa)
				pars.aa= d.getFullYear()
			
			if(!pars.mm)
				pars.mm= (d.getMonth()+1)
			
			if(!pars.dd)
				pars.dd= d.getDate()
			
		}
		/*
		if(!pars.tarea){
			pars.tarea='SRI-RECIBIDOS'
		}*/
		
		
		var req = new core.VW.Http.Request("http://127.0.0.1:" + config.port + "/site/KOI.ServerClient/api/function/c/start")
		req.method = 'POST'
		req.body = {
			año: pars.aa,
			mes: pars.mm,
			dia: pars.dd,
			force: pars.force!==false || pars.forzar!==false,
			tipo: pars.doc,
			filename: 'sri.work',
			work: pars.tarea,
			idEmpresa: pars.empresa,
			execute: true,
			preload: Path.join(__dirname,"functions","preload.js"), 
			tarea
		}
		req.contentType = 'application/json'
		req.beginGetResponse()
		req.innerRequest.on("data", function(buf) {
			var string = buf.toString()
			var Console
			if (string.indexOf("(INFO)") >= 0) {
				Console = core.VW.Console.setColorInfo()
			}
			else if (string.indexOf("(WARNING)") >= 0) {
				Console = core.VW.Console.setColorWarning()
			}
			else if (string.indexOf("(ERROR)") >= 0) {
				Console = core.VW.Console.setColorError()
			}
			else if (string.indexOf("(STATUS)") >= 0 || string.indexOf("(LOG)") >= 0) {
				Console = core.VW.Console.resetColors()
			}
			else {
				Console = core.VW.Console
			}
			Console.write(string)
		})


		await req.endGetResponse()
	}
	catch (e) {
		vw.error(e)
	}
}

ejecutarProceso()
