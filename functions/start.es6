var eval1= (function(){
	var Module= require("module").Module
	var Path= require("path")
	return function(body, code, G){
		var idSite= "/$site/" + ((G.global.context? G.global.context.idSite : "") || "anonymous")
		var filename= Path.join(idSite, body.filename)
		var module= new Module(filename, module)
		module.exports= {}
		module._compile(code, filename)
		Module._cache[filename]= module 
		return module
	}
})()
import Path from 'path'

var F
class Func{
	
	constructor(body){
		this.body= body 
	}
	
	get file(){
		return this.module.filename
	}
	
	async invoke(body){
		await this.load(body)
		return await this.execute(body)
	}
	
	async execute(body){
		if(!this.module){
			throw F.global.Exception.create("Error executing function "+ this.body.filename +". Was not loaded").putCode("INVALID")
		}
		this.func= this.module.exports(F.global)
		return await this.func(body)
	}
	
	
	async load(){
		var global= F.global 
		
		if(!this.config){
			this.config= await (global.CopyUserFunction || global.UserFunction)("config").invoke()
		}
		var config= await this.config.getConfig()
		if(this.body.execute){
			
			global.remoteExecuting= true
			global.addGlobal('remoteExecuting', true)
		
			global.remoteSite= config.codeurl
			global.addGlobal('remoteSite', config.codeurl)
			
			
			global.remoteExecutingFile= Path.join(__dirname,"..", "Command.es6")
			global.addGlobal('remoteExecutingFile', Path.join(__dirname,"..", "Command.es6"))
		
		}
		
		var url= config.codeurl + "/api/function/c/get.code"
		var req= new core.VW.Http.Request(url)	
		try{
			req.method='POST'
			req.body= this.body 
			var response= await req.getResponseAsync()
			if(response.body){
				if(typeof response.body=="string")
					response.body= JSON.parse(response.body)
				if(response.body && response.body.code){
					if(!this.func || !this.loaded || this.updated< response.body.updated){
						
						if(!this.onlyinfo)
							this.module= eval1(this.body, response.body.code, F)
						else 
							this.code= response.body.code
					}
					this.loaded= Date.now()
					this.updated= response.body.updated
				}else{
					throw global.Exception.create("Failed getting code for function: " + this.body.filename + " " + (response.body && response.body.error ? response.body.error.message : ""))
				}
			}
		}
		catch(e){
			throw e
		}
		finally{
			if(req&& req.innerRequest && req.innerRequest.callback )
				delete req.innerRequest.callback 
		}
	}
}


F = function(body) {
	
	if(body && body.execute){
		
		F.global.CopyUserFunction= F.global.UserFunction
		F.global.UserFunction = F
		
	}
	
	
	if(typeof body=="string")
		body={filename:body}
		
		
	F.global.publicContext.func_cache= F.global.publicContext.func_cache || {}
	if(body.onlyinfo){
		var j=new Func(body)
		j.onlyinfo=true 
		return j
	}
	
	
	
	if(!F.global.publicContext.func_cache[body.filename] || body.execute ){
		F.global.publicContext.func_cache[body.filename]= new Func(body)
	}
	
	
	var f= F.global.publicContext.func_cache[body.filename]
	if(body.execute){
		return f.invoke(body)
	}
	return f
}


module.exports = function(global) {
	F.global = global
	return F
}
