/*global core*/
import Path from 'path'
var Fs= core.System.IO.Fs

var F

class Config{
	
	constructor(){
		this.global= F.global 
	}
	
	async getConfig(){
		var stat, change
		var file
		
		
		if(!this.config || (this.stat && this.stat.mtime.getTime()< Date.now() - 1*60*1000)){
			// reload config ...
			
			file= Path.join(__dirname, "..", "config.json")
			if(!Fs.sync.exists(file))
				file= Path.join(this.global.UserContext.getHomeDir(), "config.json")
			
			if(!Fs.sync.exists(file))
				throw this.global.Exception.create("Archivo de configuración no encontrado").putCode("INVALID_CONFIG_FILE")
				
			if(this.stat){
				stat= this.stat 
				this.stat= await Fs.async.stat(file)	
				if(this.stat.mtime.getTime()> stat.mtime.getTime())
					change= true 
			}
			else{
				this.stat= await Fs.async.stat(file)	
				change= true 
			}
			
		}
		if(change){
			this.config= await Fs.async.readFile(file, 'utf8')
			this.code= JSON.parse(this.config)
		}
		
		return this.code
	}
}

F= function(body){
	if(!F.global.publicContext.configObject){
		F.global.publicContext.configObject= new Config()
	}
	if(!body || body.getclass){
		return F.global.publicContext.configObject 
	}
	return F.global.publicContext.configObject.getConfig()
}

module.exports= function(global){
	F.global= global 
	return F 
}